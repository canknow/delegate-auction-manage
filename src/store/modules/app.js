import assist from './../../utils/assist';
import Cookies from 'js-cookie';
import menu from '../../config/menu';
import packageJson from './../../../package';

const app = {
  namespaced: true,
  state: {
    version: packageJson.version,
    device: 'desktop',
    size: Cookies.get('size') || 'medium',

    isFullScreen: false,
    themeColor: '',
    // 面包屑数组
    menuList: [],

    multiTenancy: {
      isEnabled: true
    },

    sidebar: {
      opened: Cookies.get('sidebarStatus') ? !!+Cookies.get('sidebarStatus') : true,
      withoutAnimation: false
    },

    navShow: [],
    menus: [],
    activeMainMenuName: null,

    settings: [],
    feature: {
      allFeatures: [],
      features: []
    },
  },
  getters: {
    hashMenus (state, getters) {
      const hashMenus = {};
      assist.getHashMenus(state.menus, hashMenus);
      return hashMenus;
    }
  },
  mutations: {
    setMultiTenancy(state, multiTenancy) {
      state.multiTenancy = multiTenancy;
    },
    setAllFeatures(state, features) {
      state.feature.allFeatures = features;
    },
    setFeatures(state, features) {
      state.feature.features = features;
    },
    setSettings(state, settings) {
      state.settings = settings;
    },
    toggleDevice: (state, device) => {
      state.device = device;
    },
    setSize: (state, size) => {
      state.size = size;
      Cookies.set('size', size);
    },
    initSignalR (state) {
      state.initSignalR = true;
    },
    setOnline (state) {
      state.onlineStatus = true;
    },
    setOffline (state) {
      state.onlineStatus = false;
    },
    toggleSidebar: state => {
      state.sidebar.opened = !state.sidebar.opened;
      state.sidebar.withoutAnimation = false;
      if (state.sidebar.opened) {
        Cookies.set('sidebarStatus', 1);
      }
      else {
        Cookies.set('sidebarStatus', 0);
      }
    },
    closeSidebar: (state, withoutAnimation) => {
      Cookies.set('sidebarStatus', 0);
      state.sidebar.opened = false;
      state.sidebar.withoutAnimation = withoutAnimation;
    },
    setMenus (state, menus) {
      state.menus = menus;
    },
  },
  actions: {
    getMenus ({ state }) {
      state.menus = menu;
    }
  }
};

export default app;
