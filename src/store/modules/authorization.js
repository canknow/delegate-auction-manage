import accountService from '../../appServices/public/authorization/accountService';
import authorization from '../../application/authorization';

const authorizationStore = {
  namespaced: true,
  state: {
    userId: null
  },
  getters: {
    token (state, getters) {
      return authorization.getToken();
    }
  },
  mutations: {
    setToken (state, token) {
      authorization.setToken(token);
    }
  },
  actions: {
    getLoginOptions ({ state, commit, rootState }, data) {
      return new Promise((resolve, reject) => {
        accountService.getLoginOptions(data).then((result) => {
          resolve(result);
        }).catch(error => {
          reject(error);
        });
      });
    },
    async login ({ state, commit, rootState }, data) {
      return new Promise(async (resolve, reject) => {
        await accountService.login(data).then((result) => {
          result.tokenExpireDate = data.rememberMe ? (new Date(new Date().getTime() + 1000 * result.expireInSeconds)) : undefined;
          resolve(result);
        }).catch(error => {
          reject(error);
        });
      });
    },
    checkUserName ({ state, commit, rootState }, data) {
      return new Promise((resolve, reject) => {
        accountService.checkUserName(data).then((result) => {
          resolve(result);
        }).catch(error => {
          reject(error);
        });
      });
    }
  }
};

export default authorizationStore;
