import { messages, locale } from '../../langs';

const localization = {
  namespaced: true,
  state: {
    currentLanguageName: null,
    languages: []
  },
  mutations: {
    setLanguages (state, languages) {
      state.languages = languages;
    },
    setCurrentLanguage (state, currentLanguageName) {
      state.currentLanguageName = currentLanguageName;
    }
  },
  actions: {
    initialize ({ state, commit, rootState }, data) {
      commit('setLanguages', Object.keys(messages));
      commit('setCurrentLanguage', locale);
    }
  }
};

export default localization;
