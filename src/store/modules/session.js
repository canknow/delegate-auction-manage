import sessionService from '../../appServices/public/authorization/sessionService';

const session = {
  namespaced: true,
  state: {
    application: null,
    user: null,
    tenant: null,
    permission: {
      allPermissions: [],
      grantedPermissions: []
    },
  },
  getters: {
    shownUserName (state, getters, rootState) {
      const userName = state.user ? state.user.userName : 'Anonymous';
      if (!rootState.app.multiTenancy.isEnabled) {
        return userName;
      }
      else {
        if (state.tenant) {
          return state.tenant.tenancyName + '\\' + userName;
        }
        else {
          return '.\\' + userName;
        }
      }
    }
  },
  mutations: {
    setPermission(state, permission) {
      state.permission = permission;
    }
  },
  actions: {
    getCurrentLoginInformation ({ state }) {
      return new Promise((resolve, reject) => {
        sessionService.getCurrentLoginInformations().then((result) => {
          state.application = result.application;
          state.user = result.user;
          state.tenant = result.tenant;
          resolve(result);
        }, (error) => {
          reject(error);
        });
      });
    }
  }
};
export default session;
