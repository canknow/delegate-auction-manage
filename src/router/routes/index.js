import publicPages from './public';
import admin from './admin';
import Main from '../../views/page/Main';
import common from './common';

export const routes = [
  {
    path: '/',
    name: 'home',
    redirect: '/admin/administration/dashboard',
    component: Main,
    children: [
      ...publicPages,
      ...common,
      admin
    ]
  },
  {
    path: '*',
    redirect: '/404'
  }
];
