export default {
  path: 'team',
  name: 'Team',
  component: resolve => {
    require.ensure([], () => {
      resolve(require('../../../../views/page/admin/application/Index.vue'));
    });
  },
  meta: {
    localizationTitle: 'menu.team'
  },
  children: [
    {
      path: 'index',
      name: 'TeamIndex',
      meta: {
        localizationTitle: 'menu.team'
      },
      component: resolve => {
        require.ensure([], () => {
          resolve(require('../../../../views/page/admin/application/team/Index.vue'));
        });
      },
    }
  ]
};
