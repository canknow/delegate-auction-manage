export default {
  path: 'shopTemplate',
  name: 'ShopTemplate',
  component: resolve => {
    require.ensure([], () => {
      resolve(require('../../../../views/page/admin/application/Index.vue'));
    });
  },
  meta: {
    localizationTitle: 'menu.shopTemplate'
  },
  children: [
    {
      path: 'index',
      name: 'ShopTemplateIndex',
      meta: {
        localizationTitle: 'menu.shopTemplate'
      },
      component: resolve => {
        require.ensure([], () => {
          resolve(require('../../../../views/page/admin/application/shopTemplate/Index.vue'));
        });
      },
    },
    {
      path: 'createOrUpdate',
      name: 'ShopTemplateCreateOrUpdate',
      meta: {
        localizationTitle: 'menu.shopTemplate'
      },
      component: resolve => {
        require(['../../../../views/page/admin/application/shopTemplate/CreateOrUpdate.vue'], resolve);
      }
    },
    {
      path: 'editTemplate',
      name: 'ShopTemplateEditTemplate',
      meta: {
        localizationTitle: 'menu.shopTemplate'
      },
      component: resolve => {
        require(['../../../../views/page/admin/application/shopTemplate/EditTemplate.vue'], resolve);
      }
    }
  ]
};
