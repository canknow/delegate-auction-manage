export default {
  path: 'notice',
  name: 'Notice',
  component: resolve => {
    require(['../../../../views/page/admin/application/Index.vue'], resolve);
  },
  children: [
    {
      path: 'index',
      name: 'NoticeIndex',
      meta: {
        localizationTitle: 'menu.notice',
      },
      component: resolve => {
        require(['../../../../views/page/admin/application/notice/Index.vue'], resolve);
      }
    },
    {
      path: 'createOrUpdate',
      name: 'NoticeCreateOrUpdate',
      meta: {
        localizationTitle: 'menu.notice',
      },
      component: resolve => {
        require(['../../../../views/page/admin/application/notice/CreateOrUpdate.vue'], resolve);
      }
    }
  ]
};
