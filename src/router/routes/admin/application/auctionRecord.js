export default {
  path: 'auctionRecord',
  name: 'auctionRecord',
  component: resolve => {
    require.ensure([], () => {
      resolve(require('../../../../views/page/admin/application/Index.vue'));
    });
  },
  meta: {
    localizationTitle: 'menu.auctionRecord'
  },
  children: [
    {
      path: 'index',
      name: 'AuctionRecordIndex',
      meta: {
        localizationTitle: 'menu.auctionRecord'
      },
      component: resolve => {
        require.ensure([], () => {
          resolve(require('../../../../views/page/admin/application/auctionRecord/Index.vue'));
        });
      },
    }
  ]
};
