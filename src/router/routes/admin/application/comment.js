export default {
  path: 'comment',
  name: 'Comment',
  component: resolve => {
    require(['../../../../views/page/admin/application/Index.vue'], resolve);
  },
  children: [
    {
      path: 'index',
      meta: {
        localizationTitle: 'menu.comment',
      },
      name: 'CommentIndex',
      component: resolve => {
        require(['../../../../views/page/admin/application/comment/Index.vue'], resolve);
      }
    }
  ]
};
