import packageRoute from './packageRoute';
import team from './team';
import order from './order';
import shopTemplate from './shopTemplate';
import article from './article';
import articleCategory from './articleCategory';
import bid from './bid';
import spreadPoster from './spreadPosterTemplate';
import publicWechat from './publicWechat';
import wallet from './wallet';
import bank from './bank';
import questionAndAnswer from './questionAndAnswer';
import comment from './comment';
import shop from './shop';
import auctionAttempt from './auctionAttempt';
import customer from './customer';
import notice from './notice';
import auctionRecord from './auctionRecord';

export default [
  packageRoute,
  customer,
  team,
  order,
  shopTemplate,
  article,
  articleCategory,
  bid,
  spreadPoster,
  publicWechat,
  wallet,
  bank,
  questionAndAnswer,
  comment,
  shop,
  auctionAttempt,
  notice,
  auctionRecord
];
