export default {
  path: 'publicWechat',
  name: 'PublicWechat',
  component: resolve => {
    require(['../../../../views/page/admin/application/Index.vue'], resolve);
  },
  children: [
    {
      path: 'setting',
      name: 'PublicWechatSetting',
      redirect: '/admin/publicWechat/setting/index',
      component: resolve => {
        require(['../../../../views/page/admin/application/Index.vue'], resolve);
      },
      children: [
        {
          path: 'index',
          name: 'PublicWechat.Setting.Index',
          meta: {
            localizationTitle: 'menu.publicWechatSettings',
          },
          component: resolve => {
            require(['../../../../views/page/admin/application/publicWechat/setting/Index.vue'], resolve);
          }
        }
      ]
    },
    {
      path: 'menu',
      name: 'PublicWechatMenu',
      meta: {
        localizationTitle: 'menu.publicWechatMenu',
      },
      component: resolve => {
        require(['../../../../views/page/admin/application/publicWechat/menu/Index.vue'], resolve);
      }
    },
    {
      path: 'autoReply',
      name: 'PublicWechatAutoReply',
      redirect: '/admin/publicWechat/autoReply/index',
      component: resolve => {
        require(['../../../../views/page/admin/application/Index.vue'], resolve);
      },
      children: [
        {
          path: 'index',
          name: 'PublicWechatAutoReplyIndex',
          meta: {
            localizationTitle: 'menu.publicWechatAutoReply',
          },
          component: resolve => {
            require(['../../../../views/page/admin/application/publicWechat/autoReply/Index.vue'], resolve);
          }
        },
        {
          path: 'createOrEdit',
          name: 'PublicWechatAutoReplyCreateOrEdit',
          meta: {
            localizationTitle: 'menu.publicWechatAutoReply',
          },
          component: resolve => {
            require(['../../../../views/page/admin/application/publicWechat/autoReply/CreateOrEdit.vue'], resolve);
          }
        }
      ]
    },
    {
      path: 'file',
      name: 'PublicWechatFile',
      redirect: '/admin/publicWechat/file/picture',
      meta: {
        localizationTitle: 'menu.publicWechatFile',
      },
      component: resolve => {
        require(['../../../../views/page/admin/application/publicWechat/file/Index.vue'], resolve);
      },
      children: [
        {
          path: 'picture',
          name: 'PublicWechatFilePicture',
          meta: {
            localizationTitle: 'menu.publicWechatFilePicture',
          },
          component: resolve => {
            require(['../../../../views/page/admin/application/publicWechat/file/PicturePage.vue'], resolve);
          }
        },
        {
          path: 'video',
          name: 'PublicWechatFileVideo',
          meta: {
            localizationTitle: 'menu.publicWechatFileVideo',
          },
          component: resolve => {
            require(['../../../../views/page/admin/application/publicWechat/file/VideoPage.vue'], resolve);
          }
        },
        {
          path: 'audio',
          name: 'PublicWechatFileAudio',
          meta: {
            localizationTitle: 'menu.publicWechatFileAudio',
          },
          component: resolve => {
            require(['../../../../views/page/admin/application/publicWechat/file/AudioPage.vue'], resolve);
          }
        },
        {
          path: 'article',
          name: 'PublicWechatFileArticle',
          meta: {
            localizationTitle: 'menu.publicWechatFileArticle',
          },
          component: resolve => {
            require(['../../../../views/page/admin/application/publicWechat/file/ArticlePage.vue'], resolve);
          }
        }
      ]
    }
  ]
};
