export default {
  path: 'shop',
  name: 'shop',
  component: resolve => {
    require.ensure([], () => {
      resolve(require('../../../../views/page/admin/application/Index.vue'));
    });
  },
  meta: {
    localizationTitle: 'menu.shop'
  },
  children: [
    {
      path: 'index',
      name: 'ShopIndex',
      meta: {
        localizationTitle: 'menu.shop'
      },
      component: resolve => {
        require.ensure([], () => {
          resolve(require('../../../../views/page/admin/application/shop/Index.vue'));
        });
      },
    },
    {
      path: 'setting',
      name: 'ShopSetting',
      meta: {
        localizationTitle: 'menu.shopSetting'
      },
      component: resolve => {
        require(['../../../../views/page/admin/application/shop/Setting.vue'], resolve);
      }
    }
  ]
};
