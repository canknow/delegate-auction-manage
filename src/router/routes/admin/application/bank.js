export default {
  path: 'bank',
  name: 'FinanceBank',
  component: resolve => {
    require(['../../../../views/page/admin/application/Index.vue'], resolve);
  },
  children: [
    {
      path: 'index',
      name: 'FinanceBankIndex',
      meta: {
        localizationTitle: 'menu.bank'
      },
      component: resolve => {
        require(['../../../../views/page/admin/application/bank/Index.vue'], resolve);
      }
    },
    {
      path: 'createOrEdit',
      name: 'FinanceBankCreateOrEdit',
      meta: {
        localizationTitle: 'menu.bank'
      },
      component: resolve => {
        require(['../../../../views/page/admin/application/bank/CreateOrEdit.vue'], resolve);
      }
    }
  ]
}
