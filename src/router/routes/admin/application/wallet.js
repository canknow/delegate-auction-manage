export default {
  path: 'wallet',
  name: 'Wallet',
  redirect: '/admin/wallet/walletRecord',
  component: resolve => {
    require(['../../../../views/page/admin/application/Index.vue'], resolve);
  },
  children: [
    {
      path: 'walletRecord',
      name: 'WalletWalletRecord',
      meta: {
        localizationTitle: 'menu.walletRecord'
      },
      component: resolve => {
        require(['../../../../views/page/admin/application/wallet/walletRecord/Index.vue'], resolve);
      }
    },
    {
      path: 'withdrawApply',
      name: 'WalletWithdrawApply',
      meta: {
        localizationTitle: 'menu.withdrawApply'
      },
      component: resolve => {
        require(['../../../../views/page/admin/application/wallet/withdrawApply/Index.vue'], resolve);
      }
    }
  ]
};
