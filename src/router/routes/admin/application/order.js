export default {
  path: 'order',
  name: 'Order',
  component: resolve => {
    require.ensure([], () => {
      resolve(require('../../../../views/page/admin/application/Index.vue'));
    });
  },
  meta: {
    localizationTitle: 'menu.order'
  },
  children: [
    {
      path: 'index',
      name: 'OrderIndex',
      meta: {
        localizationTitle: 'menu.order'
      },
      component: resolve => {
        require.ensure([], () => {
          resolve(require('../../../../views/page/admin/application/order/Index.vue'));
        });
      },
    },
    {
      path: 'detail',
      name: 'OrderDetail',
      meta: {
        localizationTitle: 'menu.order'
      },
      component: resolve => {
        require.ensure([], () => {
          resolve(require('../../../../views/page/admin/application/order/Detail.vue'));
        });
      },
    }
  ]
};
