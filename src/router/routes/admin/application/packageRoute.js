export default {
  path: 'package',
  name: 'Package',
  redirect: '/package/index',
  component: resolve => {
    require.ensure([], () => {
      resolve(require('../../../../views/page/admin/application/Index.vue'));
    });
  },
  meta: {
    localizationTitle: 'menu.package'
  },
  children: [
    {
      path: 'index',
      name: 'PackageIndex',
      meta: {
        localizationTitle: 'menu.package'
      },
      component: resolve => {
        require.ensure([], () => {
          resolve(require('../../../../views/page/admin/application/package/Index.vue'));
        });
      },
    }
  ]
};
