export default {
  path: 'spreadPosterTemplate',
  name: 'SpreadPosterTemplate',
  component: resolve => {
    require(['../../../../views/page/admin/application/Index.vue'], resolve);
  },
  children: [
    {
      path: 'index',
      name: 'SpreadPosterTemplateIndex',
      meta: {
        localizationTitle: 'menu.spreadPosterTemplate'
      },
      component: resolve => {
        require(['../../../../views/page/admin/application/spreadPosterTemplate/Index.vue'], resolve);
      }
    },
    {
      path: 'createOrUpdate',
      name: 'SpreadPosterTemplateCreateOrUpdate',
      meta: {
        localizationTitle: 'menu.spreadPosterTemplate'
      },
      component: resolve => {
        require(['../../../../views/page/admin/application/spreadPosterTemplate/CreateOrUpdate.vue'], resolve);
      }
    }
  ]
};
