export default {
  path: 'customer',
  name: 'Customer',
  component: resolve => {
    require(['../../../../views/page/admin/application/Index.vue'], resolve);
  },
  children: [
    {
      path: 'index',
      name: 'CustomerIndex',
      meta: {
        localizationTitle: 'menu.customer',
      },
      component: resolve => {
        require(['../../../../views/page/admin/application/customer/Index.vue'], resolve);
      }
    },
    {
      path: 'createOrEdit',
      name: 'CustomerCreateOrEdit',
      meta: {
        localizationTitle: 'menu.customer',
      },
      component: resolve => {
        require(['../../../../views/page/admin/application/customer/CreateOrEdit.vue'], resolve);
      }
    }
  ]
};
