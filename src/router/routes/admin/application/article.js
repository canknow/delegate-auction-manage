export default {
  path: 'article',
  name: 'Article',
  component: resolve => {
    require(['../../../../views/page/admin/application/Index.vue'], resolve);
  },
  children: [
    {
      path: 'index',
      meta: {
        localizationTitle: 'menu.article',
      },
      name: 'ArticleIndex',
      component: resolve => {
        require(['../../../../views/page/admin/application/article/Index.vue'], resolve);
      }
    },
    {
      path: 'createOrUpdate',
      name: 'ArticleCreateOrUpdate',
      meta: {
        localizationTitle: 'menu.article',
      },
      component: resolve => {
        require(['../../../../views/page/admin/application/article/CreateOrUpdate.vue'], resolve);
      }
    }
  ]
};
