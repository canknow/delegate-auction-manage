export default {
  path: 'articleCategory',
  name: 'ArticleCategory',
  component: resolve => {
    require(['../../../../views/page/admin/application/Index.vue'], resolve);
  },
  children: [
    {
      path: 'index',
      name: 'ArticleCategoryIndex',
      meta: {
        localizationTitle: 'menu.articleCategory',
      },
      component: resolve => {
        require(['../../../../views/page/admin/application/articleCategory/Index.vue'], resolve);
      }
    }
  ]
};
