export default {
  path: 'questionAndAnswer',
  name: 'questionAndAnswer',
  component: resolve => {
    require.ensure([], () => {
      resolve(require('../../../../views/page/admin/application/Index.vue'));
    });
  },
  meta: {
    localizationTitle: 'menu.questionAndAnswer'
  },
  children: [
    {
      path: 'index',
      name: 'QuestionAndAnswerIndex',
      meta: {
        localizationTitle: 'menu.questionAndAnswer'
      },
      component: resolve => {
        require.ensure([], () => {
          resolve(require('../../../../views/page/admin/application/questionAndAnswer/Index.vue'));
        });
      },
    }
  ]
};
