export default {
  path: 'bid',
  name: 'Bid',
  component: resolve => {
    require.ensure([], () => {
      resolve(require('../../../../views/page/admin/application/Index.vue'));
    });
  },
  meta: {
    localizationTitle: 'menu.bid'
  },
  children: [
    {
      path: 'index',
      name: 'BidIndex',
      meta: {
        localizationTitle: 'menu.bid'
      },
      component: resolve => {
        require.ensure([], () => {
          resolve(require('../../../../views/page/admin/application/bid/Index.vue'));
        });
      },
    }
  ]
};
