export default {
  path: 'auctionAttempt',
  name: 'auctionAttempt',
  component: resolve => {
    require.ensure([], () => {
      resolve(require('../../../../views/page/admin/application/Index.vue'));
    });
  },
  meta: {
    localizationTitle: 'menu.auctionAttempt'
  },
  children: [
    {
      path: 'index',
      name: 'AuctionAttemptIndex',
      meta: {
        localizationTitle: 'menu.auctionAttempt'
      },
      component: resolve => {
        require.ensure([], () => {
          resolve(require('../../../../views/page/admin/application/auctionAttempt/Index.vue'));
        });
      },
    },
    {
      path: 'CreateOrUpdate',
      name: 'AuctionAttemptCreateOrUpdate',
      meta: {
        localizationTitle: 'menu.auctionAttempt'
      },
      component: resolve => {
        require.ensure([], () => {
          resolve(require('../../../../views/page/admin/application/auctionAttempt/CreateOrUpdate.vue'));
        });
      },
    }
  ]
};
