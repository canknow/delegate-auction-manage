import administration from './administration';
import application from './application/index';
import common from './common';
import AdminLayout from '../../../views/page/admin/layout/Index';

export default {
  path: 'admin',
  component: AdminLayout,
  meta: { allowAnonymous: true },
  name: 'Admin',
  children: [
    administration,
    ...application,
    ...common
  ]
};
