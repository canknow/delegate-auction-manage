const namePrefix = 'Administration.';

export default {
  path: 'administration',
  name: 'Administration',
  redirect: '/administration/home',
  component: resolve => {
    require.ensure([], () => {
      resolve(require('../../../views/page/admin/administration/Index.vue'));
    });
  },
  children: [
    {
      path: 'dashboard',
      name: namePrefix + 'Dashboard',
      meta: {
        localizationTitle: 'menu.dashboard'
      },
      component: resolve => {
        require(['../../../views/page/admin/administration/dashboard/Index.vue'], resolve);
      },
      children: [
        {
          path: 'host',
          name: namePrefix + 'Dashboard.Host',
          meta: {
            localizationTitle: 'menu.dashboard'
          },
          component: resolve => {
            require.ensure([], () => {
              resolve(require('../../../views/page/admin/administration/dashboard/host/Index.vue'));
            });
          }
        },
        {
          path: 'tenant',
          name: namePrefix + 'Dashboard.Tenant',
          meta: {
            localizationTitle: 'menu.dashboard'
          },
          component: resolve => {
            require.ensure([], () => {
              resolve(require('../../../views/page/admin/administration/dashboard/tenant/Index.vue'));
            });
          }
        }
      ]
    },
    {
      path: 'profile',
      name: 'profile',
      component: resolve => {
        require(['../../../views/page/admin/application/Index.vue'], resolve);
      },
      children: [
        {
          path: 'changePassword',
          name: 'changePassword',
          meta: {
            localizationTitle: 'menu.profile.changePassword'
          },
          component: resolve => {
            require(['../../../views/page/public/account/ChangePassword'], resolve);
          }
        },
        {
          path: 'userLoginAttempt',
          name: 'userLoginAttempt',
          meta: {
            localizationTitle: 'menu.account.userLoginAttempt'
          },
          component: resolve => {
            require(['../../../views/page/public/account/UserLoginAttempt'], resolve);
          }
        },
        {
          path: 'index',
          name: 'index',
          meta: {
            localizationTitle: 'menu.profile.profile'
          },
          component: resolve => {
            require(['../../../views/page/public/account/Profile'], resolve);
          }
        }
      ]
    },
    { // 角色信息
      path: 'organizationUnits',
      name: namePrefix + 'OrganizationUnits',
      meta: {
        localizationTitle: 'menu.organizationUnits'
      },
      component: resolve => {
        require.ensure([], () => {
          resolve(require('../../../views/page/admin/administration/organizationUnits/Index.vue'));
        });
      }
    },
    { // 角色信息
      path: 'role',
      name: namePrefix + 'Role',
      meta: {
        permission: 'Pages.Administration.Roles',
        localizationTitle: 'menu.role'
      },
      component: resolve => {
        require.ensure([], () => {
          resolve(require('../../../views/page/admin/administration/role/Index.vue'));
        });
      }
    },
    {
      path: 'user',
      name: namePrefix + 'User',
      component: resolve => {
        require.ensure([], () => {
          resolve(require('../../../views/page/admin/administration/user/Index.vue'));
        });
      },
      meta: {
        permission: 'Pages.Administration.Users',
        localizationTitle: 'menu.user'
      }
    },
    {
      path: 'tenant',
      name: namePrefix + 'Tenants',
      component: resolve => {
        require.ensure([], () => {
          resolve(require('../../../views/page/admin/administration/tenant/Index.vue'));
        });
      },
      meta: {
        permission: 'Pages.Administration.Tenants',
        localizationTitle: 'menu.tenant'
      }
    },
    { // 审计日志
      path: 'auditLog',
      name: namePrefix + 'AuditLog',
      meta: {
        localizationTitle: 'menu.auditLog'
      },
      component: resolve => {
        require.ensure([], () => {
          resolve(require('../../../views/page/admin/administration/auditLog/Index.vue'));
        });
      }
    },
    {
      path: 'backgroundJob',
      name: namePrefix + 'BackgroundJob',
      meta: {
        localizationTitle: 'menu.backgroundJob'
      },
      component: resolve => {
        require.ensure([], () => {
          resolve(require('../../../views/page/admin/administration/backgroundJob/Index.vue'));
        });
      }
    },
    {
      path: 'edition',
      name: namePrefix + 'Edition',
      meta: {
        localizationTitle: 'menu.edition'
      },
      component: resolve => {
        require.ensure([], () => {
          resolve(require('../../../views/page/admin/administration/edition/Index.vue'));
        });
      }
    },
    {
      path: 'file',
      name: namePrefix + 'File',
      meta: {
        localizationTitle: 'menu.file'
      },
      component: resolve => {
        require.ensure([], () => {
          resolve(require('../../../views/page/admin/administration/file/Index.vue'));
        });
      }
    },
    { // Maintenance
      path: 'maintenance',
      name: namePrefix + 'Maintenance',
      meta: {
        localizationTitle: 'menu.maintenance'
      },
      component: resolve => {
        require.ensure([], () => {
          resolve(require('../../../views/page/admin/administration/maintenance/Index.vue'));
        });
      }
    },
    { // Maintenance
      path: 'file',
      name: namePrefix + 'File',
      meta: {
        localizationTitle: 'menu.file'
      },
      component: resolve => {
        require.ensure([], () => {
          resolve(require('../../../views/page/components/FileManage.vue'));
        });
      }
    },
    { // Maintenance
      path: 'captcha',
      name: namePrefix + 'Captcha',
      meta: {
        localizationTitle: 'menu.captcha'
      },
      component: resolve => {
        require.ensure([], () => {
          resolve(require('../../../views/page/admin/administration/captcha/Index.vue'));
        });
      }
    },
    {
      path: 'setting',
      name: namePrefix + 'Setting',
      component: resolve => {
        require(['../../../views/page/admin/application/Index.vue'], resolve);
      },
      children: [
        {
          path: 'host',
          name: namePrefix + 'Setting.Host',
          meta: {
            localizationTitle: 'menu.setting'
          },
          component: resolve => {
            require.ensure([], () => {
              resolve(require('../../../views/page/admin/administration/setting/Host.vue'));
            });
          }
        },
        {
          path: 'tenant',
          name: namePrefix + 'Setting.Tenant',
          meta: {
            localizationTitle: 'menu.setting'
          },
          component: resolve => {
            require.ensure([], () => {
              resolve(require('../../../views/page/admin/administration/setting/Tenant.vue'));
            });
          }
        }
      ]
    },
    {
      path: 'businessNotifier',
      name: namePrefix + 'BusinessNotifier',
      component: resolve => {
        require(['../../../views/page/admin/application/Index.vue'], resolve);
      },
      redirect: '/admin/administration/businessNotifier/index',
      children: [
        {
          path: 'index',
          name: namePrefix + 'BusinessNotifier.Index',
          meta: {
            localizationTitle: 'menu.businessNotifier'
          },
          component: resolve => {
            require(['../../../views/page/admin/administration/businessNotifier/Index.vue'], resolve);
          }
        },
        {
          path: 'createOrEdit',
          name: namePrefix + 'BusinessNotifier.CreateOrEdit',
          meta: {
            localizationTitle: 'menu.businessNotifier'
          },
          component: resolve => {
            require(['../../../views/page/admin/administration/businessNotifier/CreateOrEdit.vue'], resolve);
          }
        }
      ]
    }
  ]
};
