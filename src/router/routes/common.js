export default [
  {
    path: '/404',
    name: '404',
    meta: { allowAnonymous: true, title: 'Page does not exist' },
    component: resolve => {
      require(['../../views/page/common/ErrorPage'], resolve);
    }
  },
  {
    path: '/403',
    name: '403',
    meta: { allowAnonymous: true, title: 'You are not authorized' },
    component: resolve => {
      require(['../../views/page/common/ErrorPage'], resolve);
    }
  },
  {
    path: '/500',
    name: '500',
    meta: { allowAnonymous: true, title: 'Server error' },
    component: resolve => {
      require(['../../views/page/common/ErrorPage'], resolve);
    }
  },
  {
    path: '/result',
    name: 'result',
    meta: { allowAnonymous: true },
    component: resolve => {
      require(['../../views/page/common/ResultPage'], resolve);
    }
  }
];
