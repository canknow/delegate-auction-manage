import account from './account';

export default [
  account,
  {
    path: '/install',
    name: 'install',
    meta: { allowAnonymous: true },
    component: resolve => {
      require(['../../../views/page/public/install/Index'], resolve);
    }
  }
];
