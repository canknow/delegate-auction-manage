import Main from '../../../views/page/Main';

export default {
  path: 'account',
  name: 'Account',
  component: Main,
  children: [
    {
      path: 'author',
      name: 'Account.Author',
      meta: { allowAnonymous: true },
      component: resolve => {
        require(['../../../views/page/public/account/Author'], resolve);
      }
    },
    {
      path: 'login',
      name: 'Account.Login',
      meta: { allowAnonymous: true, title: 'LogIn' },
      component: resolve => {
        require(['../../../views/page/public/account/Login'], resolve);
      }
    },
    {
      path: 'select-edition',
      name: 'Account.SelectEdition',
      meta: { allowAnonymous: true, title: 'SelectEdition' },
      component: resolve => {
        require(['../../../views/page/public/account/SelectEdition'], resolve);
      }
    },
    {
      path: 'register-tenant',
      name: 'Account.RegisterTenant',
      meta: { allowAnonymous: true, title: 'Register' },
      component: resolve => {
        require(['../../../views/page/public/account/RegisterTenant'], resolve);
      }
    },
    {
      path: 'register-tenant-result',
      name: 'Account.RegisterTenantResult',
      meta: { allowAnonymous: true, title: 'Register' },
      component: resolve => {
        require(['../../../views/page/public/account/RegisterTenantResult'], resolve);
      }
    },
    {
      path: 'buy',
      name: 'Account.Buy',
      meta: { allowAnonymous: true, title: 'Buy' },
      component: resolve => {
        require(['../../../views/page/public/payment/Buy'], resolve);
      }
    },
    {
      path: 'forgetPassword',
      name: 'Account.ForgetPassword',
      meta: { allowAnonymous: true, title: 'ForgetPassword' },
      component: resolve => {
        require(['../../../views/page/public/account/ForgetPassword'], resolve);
      }
    },
    {
      path: 'emailActivation',
      name: 'Account.EmailActivation',
      meta: { allowAnonymous: true, title: 'EmailActivation' },
      component: resolve => {
        require(['../../../views/page/public/account/EmailActivation'], resolve);
      }
    }
  ]
};
