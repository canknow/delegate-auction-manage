import Vue from 'vue';
import VueRouter from 'vue-router';
import canknow from 'canknow';
import { routes } from './routes';
import authorization from './../application/authorization';
import assist from './../utils/assist';

Vue.use(VueRouter);

const routerConfig = {
  // mode: 'history',
  routes
};

VueRouter.prototype.goBack = function () {
  this.isBack = true;
  window.history.go(-1);
};

const router = new VueRouter(routerConfig);

const gotoAuthor = (to, from, next) => {
  if (from.path !== '/account/author') {
    localStorage.setItem('authorReturnUrl', to.fullPath);
  }
  next({ path: '/account/author' });
};

router.beforeEach((to, from, next) => {
  canknow.LoadingBar.start();
  assist.setPageTitle(to.meta.title);
  if (!(to.matched[0].name === 'Home') && (!to.meta.allowAnonymous)) {
    if (authorization.isAuthorized()) {
      if (to.path === '/account/login') {
        next({ path: '/' });
      }
      else {
        next();
      }
    }
    else {
      gotoAuthor(to, from, next);
    }
  }
  else {
    next();
  }
});

// 路由跳转后操作（结束进度条特效）
router.afterEach((to, from, next) => {
  canknow.LoadingBar.finish();
  window.scrollTo(0, 0);
});
export default router;
