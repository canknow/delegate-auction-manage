import store from './../store';

export default (value) => {
  if (!value) {
    return value;
  }
  return store.getters.baseUrl + 'system/pictureproxy?url=' + value;
};
