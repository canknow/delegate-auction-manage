import requestTypes from '../views/page/admin/application/publicWechat/autoReply/requestTypes';
export default (value) => {
  const key = Object.keys(requestTypes).find((key) => {
    return requestTypes[key] === value;
  });
  return key;
};
