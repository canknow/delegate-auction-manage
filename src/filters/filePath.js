import store from './../store';

export default (value) => {
  if (!value) {
    return value;
  }
  if (value.includes('//') || value.includes('data:')) {
    if (value.includes('http://mmbiz.qpic.cn')) {
      value = store.getters.baseUrl + '/system/pictureproxy?url=' + value;
    }
    return value;
  }
  return store.getters.baseUrl + value;
};
