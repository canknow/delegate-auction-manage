import responseMsgTypes from '../views/page/admin/application/publicWechat/autoReply/responseMsgTypes';
export default (value) => {
  const key = Object.keys(responseMsgTypes).find((key) => {
    return responseMsgTypes[key] === value;
  });
  return key;
};
