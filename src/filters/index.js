import toPercent from './toPercent';
import localize from './localize';
import money from './money';
import filePath from './filePath';
import fileExtension from './fileExtension';
import proxyFilePath from './proxyFilePath';
import notificationTitle from './notificationTitle';
import requestTypeParse from './requestTypeParse';
import responseMsgTypeParse from './responseMsgTypeParse';

export default {
  toPercent,
  localize,
  money,
  filePath,
  fileExtension,
  proxyFilePath,
  notificationTitle,
  requestTypeParse,
  responseMsgTypeParse
};
