import fetch from './fetch';
import assist from './../utils/assist';

export default class AbpAppService{
  constructor (name, moduleName = 'admin') {
    this.name = name;
    this.moduleName = moduleName;
    this.baseUrl = assist.getInterfaceUrl();
    this.initialize();
  }
  get pascalCaseName () {
    return this.name.toPascalCase();
  }
  initialize () {
    this.buildAction('get', 'get');
    this.buildAction(`get${this.pascalCaseName}`, 'get');
    this.buildAction(`get${this.pascalCaseName}ForCreateOrEdit`, 'get');
    this.buildAction(`get${this.pascalCaseName}ForEdit`, 'get');
    this.buildAction(`getDefault${this.pascalCaseName}`, 'get');
    this.buildAction('gets', 'get');
    this.buildAction('getAll', 'get');
    this.buildAction(`get${this.pascalCaseName}Settings`, 'get');

    this.buildAction('create', 'post');
    this.buildAction(`create${this.pascalCaseName}`, 'post');
    this.buildAction('update', 'put');
    this.buildAction(`update${this.pascalCaseName}`, 'put');
    this.buildAction(`update${this.pascalCaseName}Settings`, 'put');
    this.buildAction('createOrEdit', 'post');
    this.buildAction(`createOrUpdate${this.pascalCaseName}`, 'post');
    this.buildAction(`createOrUpdate${this.pascalCaseName}Settings`, 'post');

    this.buildAction('delete', 'delete');
    this.buildAction('setAsDefault', 'post');
    this.buildAction('install', 'post');
  }
  get appServiceBaseUrl () {
    return this.baseUrl + `/api/services/${this.moduleName}/` + this.pascalCaseName + '/';
  }
  getAppServiceUrl (action) {
    return this.appServiceBaseUrl + action;
  }
  get appControllerBaseUrl () {
    return this.baseUrl + `/${this.moduleName}/` + this.pascalCaseName + '/';
  }
  getControllerActionUrl (action) {
    return this.appControllerBaseUrl + action;
  }
  buildControllerAction(action, method = 'post') {
    this[action] = async function (data) {
      switch (method) {
        case 'get':
        case 'head':
        case 'delete':
          return this.controllerAction(action, method, { params: data });
        case 'post':
        case 'put':
        case 'patch':
          return this.controllerAction(action, method, data);
      }
    };
  }
  buildAction (action, method = 'post') {
    this[action] = async function (data) {
      switch (method) {
        case 'get':
        case 'head':
        case 'delete':
          return this.action(action, method, { params: data });
        case 'post':
        case 'put':
        case 'patch':
          return this.action(action, method, data);
      }
    };
  }
  buildActions (actions, method = 'post') {
    actions.forEach((action) => {
      this.buildAction(action, method);
    });
  }
  async controllerAction (action, method, data) {
    if (data && data.params && data.params.pageSize) {
      data.params.pageIndex = !data.params.pageIndex ? 1 : data.params.pageIndex;
      data.params.skipCount = data.params.pageSize * (data.params.pageIndex - 1);
      data.params.maxResultCount = data.params.pageSize;
    }
    switch (method) {
      case 'get':
      case 'delete':
        return fetch[method](this.getControllerActionUrl(action.toPascalCase()), data);
      case 'post':
      case 'put':
        return fetch[method](this.getControllerActionUrl(action.toPascalCase()), data);
    }
  }
  async action (action, method, data) {
    if (data && data.params && data.params.pageSize) {
      data.params.pageIndex = !data.params.pageIndex ? 1 : data.params.pageIndex;
      data.params.skipCount = data.params.pageSize * (data.params.pageIndex - 1);
      data.params.maxResultCount = data.params.pageSize;
    }
    switch (method) {
      case 'get':
      case 'delete':
        return fetch[method](this.getAppServiceUrl(action.toPascalCase()), data);
      case 'post':
      case 'put':
        return fetch[method](this.getAppServiceUrl(action.toPascalCase()), data);
    }
  }
}
