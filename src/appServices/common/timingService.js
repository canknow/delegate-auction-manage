import AbpAppService from '../AbpAppService';
const service = new AbpAppService('timing');
service.buildAction('getTimezones', 'get');
service.buildAction('getTimezoneComboboxItems', 'get');
service.buildAction('getTimezoneInfos', 'get');
export default service;
