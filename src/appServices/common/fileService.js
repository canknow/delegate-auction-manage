import config from '../../config/app';
import assist from '../../utils/assist';
import fetch from './../fetch';

const service = {};
service.downloadTempFile = ({ fileType, fileToken, fileName }) => {
  const newPage = window.open('about:blank');
  newPage.location.href = `${config.baseUrl}/File/DownloadTempFile?fileType=${fileType}&fileToken=${fileToken}&fileName=${fileName}`;
};
service.getToken = function async () {
  return fetch.get(assist.getInterfaceUrl() + '/Application/GetCloudStoreToken');
};
export default service;
