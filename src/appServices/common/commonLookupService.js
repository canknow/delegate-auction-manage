import AbpAppService from '../AbpAppService';
const service = new AbpAppService('CommonLookup', 'public');
service.buildAction('getEditionsForCombobox', 'get');
export default service;
