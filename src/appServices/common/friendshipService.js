import AbpAppService from '../AbpAppService';
const service = new AbpAppService('friendship', 'public');
service.buildAction('blockUser', 'post');
service.buildAction('unblockUser', 'post');
service.buildAction('createFriendshipRequest', 'post');
service.buildAction('createFriendshipRequestByUserName', 'post');
export default service;
