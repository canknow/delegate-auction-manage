import AbpAppService from '../../AbpAppService';
const service = new AbpAppService('tenantSettings');
service.buildAction('getAllSettings', 'get');
service.buildAction('updateAllSettings', 'put');
export default service;
