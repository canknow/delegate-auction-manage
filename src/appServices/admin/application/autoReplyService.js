import AbpAppService from '../../AbpAppService';
const service = new AbpAppService('autoReply');
service.buildAction('getAutoReplyForCreateOrEdit', 'get');
export default service;
