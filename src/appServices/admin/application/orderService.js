import AbpAppService from '../../AbpAppService';
const service = new AbpAppService('order');
service.buildAction('pass', 'post');
service.buildAction('deny', 'post');
service.buildAction('pay', 'post');
service.buildAction('remark', 'post');
service.buildAction('compensate', 'post');
export default service;
