import AbpAppService from '../../AbpAppService';
const service = new AbpAppService('publicWechatSettings');
service.buildAction('getSettings', 'get');
service.buildAction('updateGeneral', 'put');
service.buildAction('updatePay', 'put');
service.buildAction('installTemplateMessageTempalte', 'post');
service.buildAction('updateCustomerService', 'put');
service.buildAction('installCertificate', 'post');
export default service;
