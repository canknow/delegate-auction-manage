import AbpAppService from '../../AbpAppService';
import fileService from '../../common/fileService';
const service = new AbpAppService('withdrawApplyService');
service.buildAction('getWithdrawApplyToExcel', 'get');
service.buildAction('getWithdrawApplyAllOfPage', 'get');
service.buildAction('withdraw', 'post');
service.export = async function (params) {
  let ret = await service.getWithdrawApplyToExcel(params);
  fileService.downloadTempFile(ret);
};
export default service;
