import AbpAppService from '../../AbpAppService';
const service = new AbpAppService('auctionAttempt');
service.buildAction('createAsync', 'post');
export default service;
