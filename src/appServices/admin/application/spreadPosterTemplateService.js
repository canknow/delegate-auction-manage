import AbpAppService from '../../AbpAppService';
const service = new AbpAppService('spreadPosterTemplate');
service.buildAction('getSpreadPosterTemplateForCreateOrEdit', 'get');
service.buildAction('toggleStatus', 'post');
export default service;
