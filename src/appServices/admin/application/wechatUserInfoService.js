import AbpAppService from '../../../AbpAppService';
const service = new AbpAppService('wechatUserInfo');
service.buildAction('getTags', 'get');
service.buildAction('getGroups', 'get');
export default service;
