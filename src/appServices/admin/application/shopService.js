import AbpAppService from '../../AbpAppService';
const service = new AbpAppService('shop');
service.buildAction('getShop', 'get');
export default service;
