import AbpAppService from '../../AbpAppService';
const service = new AbpAppService('hostSettings');
service.buildAction('getAllSettings', 'get');
service.buildAction('updateAllSettings', 'put');
service.buildAction('sendTestEmail', 'post');
service.buildAction('sendTestSms', 'post');
export default service;
