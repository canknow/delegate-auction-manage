import AbpAppService from '../../AbpAppService';
const service = new AbpAppService('customers');
service.buildAction('setAsSpreader', 'post');
service.buildAction('toggleActive', 'post');
service.buildAction('getCustomerDetail', 'get');
service.buildAction('getUserLocations', 'get');
service.buildAction('bindParent', 'post');
service.buildAction('resetPassword', 'post');
export default service;
