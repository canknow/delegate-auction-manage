import AbpAppService from '../../AbpAppService';
const service = new AbpAppService('wechatPay');
service.buildAction('payToUser', 'post');
export default service;
