import AbpAppService from '../../AbpAppService';
const service = new AbpAppService('shopTemplate');
service.buildAction('createOrUpdateShopTemplate', 'post');
service.buildAction('getShopTemplateForCreateOrEdit', 'get');
export default service;
