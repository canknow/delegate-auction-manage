import AbpAppService from '../../AbpAppService';
const service = new AbpAppService('caching');
service.buildAction('getAllCaches', 'get');
service.buildAction('clearAllCaches', 'post');
export default service;
