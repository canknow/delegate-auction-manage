import AbpAppService from '../../AbpAppService';
const service = new AbpAppService('edition');
service.buildAction('getEditions', 'get');
service.buildAction('getEditionForEdit', 'get');
service.buildAction('getFeatures', 'get');
export default service;
