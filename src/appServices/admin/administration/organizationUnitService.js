import AbpAppService from '../../AbpAppService';
const service = new AbpAppService('organizationUnit');
service.buildAction('getOrganizationUnits', 'get');
service.buildAction('getOrganizationUnitUsers', 'get');
service.buildAction('createOrganizationUnit', 'post');
service.buildAction('updateOrganizationUnit', 'put');
service.buildAction('addUsersToOrganizationUnit', 'post');
service.buildAction('deleteOrganizationUnit', 'delete');
export default service;
