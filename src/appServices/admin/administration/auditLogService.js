import AbpAppService from '../../AbpAppService';
const service = new AbpAppService('AuditLog');
service.buildAction('getAuditLogs', 'get');
export default service;
