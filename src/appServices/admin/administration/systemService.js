import AbpAppService from '../../AbpAppService';
const service = new AbpAppService('system');
service.buildAction('initializeData', 'post');
export default service;
