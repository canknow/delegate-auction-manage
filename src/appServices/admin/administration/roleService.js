import AbpAppService from '../../AbpAppService';
const service = new AbpAppService('role');
service.buildAction('getRoles', 'get');
service.buildAction('getRoleForEdit', 'get');
service.buildAction('deleteRole', 'delete');
export default service;
