import AbpAppService from '../../AbpAppService';
import fetch from '../../fetch';
const service = new AbpAppService('systemMonitor');
service.buildAction('getSystemInfo', 'get');
service.getMetrics = async () => {
  return new Promise((resolve, reject) => {
    fetch.get('metrics').then((result) => {
      resolve(result);
    });
  });
};
export default service;
