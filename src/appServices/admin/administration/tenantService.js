import AbpAppService from '../../AbpAppService';
const service = new AbpAppService('tenant');
service.buildAction('getTenants', 'get');
service.buildAction('getTenantForEdit', 'get');
service.buildAction('createOrMigrateForTenant', 'post');
export default service;
