import AbpAppService from '../../AbpAppService';
const service = new AbpAppService('User');
service.buildAction('getUsers', 'get');
service.buildAction('getUserForEdit', 'get');
service.buildAction('unlockUser', 'post');
service.buildAction('deleteUser', 'delete');
export default service;
