import AbpAppService from '../../AbpAppService';
const service = new AbpAppService('permission');
service.buildAction('getAllPermissions', 'get');
export default service;
