import AbpAppService from '../../AbpAppService';
import fileService from './../../common/fileService';

const service = new AbpAppService('webLog');
service.buildAction('downloadWebLogs', 'get');
service.buildAction('getLatestWebLogs', 'get');
service.buildAction('deleteWebLogs', 'delete');
service.exportAll = async function () {
  let ret = await this.downloadWebLogs();
  fileService.downloadTempFile(ret);
};
export default service;
