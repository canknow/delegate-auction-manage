import AbpAppService from '../AbpAppService';
const service = new AbpAppService('UserConfiguration', 'public');
service.buildAction('getAll', 'get');
service.buildAction('getUserMultiTenancyConfig', 'get');
service.buildAction('getUserSessionConfig', 'get');
service.buildAction('getUserFeaturesConfig', 'get');
service.buildAction('getUserAuthConfig', 'get');
service.buildAction('getUserSettingConfig', 'get');
export default service;
