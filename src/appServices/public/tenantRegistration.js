import fetch from '../fetch';

export default {
  index (data) {
    return fetch.post('/TenantRegistration/Index', data);
  },
  register (data) {
    return fetch.post('/TenantRegistration/Register', data);
  },
  sendVerificationCode (data) {
    return fetch.post('/TenantRegistration/SendVerificationCode', data);
  }
};
