import AbpAppService from '../AbpAppService';
const service = new AbpAppService('Install', 'public');
service.buildAction('getAppSettingsJson', 'get');
service.buildAction('checkDatabase', 'post');
service.buildAction('setup', 'post');
export default service;
