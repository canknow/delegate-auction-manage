import AbpAppService from '../AbpAppService';
const service = new AbpAppService('tenantRegistration', 'public');
service.buildAction('getTenantRegistrationOptions', 'get');
service.buildAction('registerTenant', 'post');
service.buildAction('getEditionsForSelect', 'get');
service.buildAction('getEdition', 'get');
export default service;
