import AbpAppService from '../../AbpAppService';
const service = new AbpAppService('Account');
service.buildControllerAction('login', 'post');
service.buildControllerAction('logout', 'post');
service.buildControllerAction('sendPasswordResetCode', 'post');
service.buildControllerAction('sendEmailActivationLink', 'post');
service.buildControllerAction('getLoginOptions', 'get');
service.buildControllerAction('checkUserName', 'post');
export default service;
