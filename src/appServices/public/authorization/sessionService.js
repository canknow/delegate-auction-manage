import AbpAppService from '../../AbpAppService';
const service = new AbpAppService('session');
service.buildAction('getShopInformations', 'get');
service.buildAction('getCurrentLoginInformations', 'get');
export default service;
