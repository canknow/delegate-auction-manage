import AbpAppService from '../../AbpAppService';
const service = new AbpAppService('profile');
service.buildAction('uploadPortrait', 'post');
service.buildAction('changePassword', 'post');
service.buildAction('updateCurrentUserProfile', 'put');
service.buildAction('getPasswordComplexitySetting', 'get');
export default service;
