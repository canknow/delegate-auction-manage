import axios from 'axios';
import assist from '../utils/assist';
import canknow from 'canknow';
import cookie from 'js-cookie';
import authorization from './../application/authorization';

const ajax = axios.create({
  baseURL: assist.getInterfaceUrl(), // WebApi的前缀base_uri路径
  timeout: 60000 // 请求超时时间
});

ajax.defaults.withCredentials = true;

ajax.interceptors.request.use(config => {
  if (authorization.getToken()) { // 判断是否存在token，如果存在的话，则每个http header都加上token
    config.headers.Authorization = `Bearer ${authorization.getToken()}`;
  }
  config.headers.common['.AspNetCore.Culture'] = cookie.get('Abp.Localization.CultureName');
  config.headers.common['Abp.TenantId'] = cookie.get('Abp.TenantId');
  return config;
}, function (error) {
  return Promise.reject(error);
});

ajax.interceptors.response.use(
  response => {
    if (response.data.success) {
      return Promise.resolve(response.data.result);
    }
    else {
      canknow.Message.error(response.data.error.details, response.data.error.message);
      return Promise.reject(response.data.error.message);
    }
  },
  error => {
    let message = error.message;
    if (error.response) {
      message = error.response.statusText;
      if (error.response.data && error.response.data.error) {
        message = error.response.data.error.message;
      }
      if (error.response.data.unAuthorizedRequest) {
        authorization.logout();
      }
    }
    canknow.Message.error(message);
    return Promise.reject(error);
  }
);
export default ajax;
