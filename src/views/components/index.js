import PictureUploader from './PictureUploader';
import PicturesUploader from './PicturesUploader';
import Profile from './Profile';
import Editor from './Editor';
import FileUploader from './FileUploader';
import InputFileUploader from './InputFileUploader';
import AdminPageContentHeader from './AdminPageContentHeader';
import VerificationCodeInput from './VerificationCodeInput';
import LocationMap from './LocationMap';
import LinkInput from './LinkInput';
import PhonePreview from './PhonePreview';
import PublicWechatEventHandleSelector from './PublicWechatEventHandleSelector';

export default {
  PictureUploader,
  FileUploader,
  InputFileUploader,
  PicturesUploader,
  Profile,
  Editor,
  AdminPageContentHeader,
  VerificationCodeInput,
  LocationMap,
  LinkInput,
  PhonePreview,
  PublicWechatEventHandleSelector
};
