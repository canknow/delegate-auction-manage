import fileService from '../../appServices/common/fileService';

export default {
  props: {
    useQiniu: {
      type: Boolean,
      default: false
    },
    qiniuUrl: {
      type: String
    }
  },
  data () {
    return {
      baseUrl: this.useQiniu ? this.qiniuUrl : this.$store.getters.baseUrl
    };
  },
  methods: {
    resultParse (response) {
      return this.useQiniu ? response.result.path : response.key;
    },
    getToken () {
      fileService.getToken().then((result) => {
        this.uploadData.token = result;
      });
    }
  },
  created () {
    if (this.useQiniu) {
      this.getToken();
    }
  }
};
