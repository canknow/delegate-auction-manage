export default [
  { name: 'click', title: '点击推事件' },
  { name: 'view', title: '跳转URL' },
  { name: 'scancode_push', title: '扫码推事件' },
  { name: 'scancode_waitmsg', title: '扫码推事件且弹出“消息接收中”提示框' },
  { name: 'pic_sysphoto', title: '弹出系统拍照发图' },
  { name: 'pic_photo_or_album', title: '弹出拍照或者相册发图' },
  { name: 'pic_weixin', title: '弹出微信相册发图器' },
  { name: 'location_select', title: '弹出地理位置选择器' },
  { name: 'media_id', title: '下发消息（除文本消息）' },
  { name: 'view_limited', title: '跳转图文消息URL' }
];
