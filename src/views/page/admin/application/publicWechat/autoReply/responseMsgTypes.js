export default {
  TextMessage: 0,
  NewsMessage: 1,
  MusicMessage: 2,
  ImageMessage: 3,
  VoiceMessage: 4,
  VideoMessage: 5,
  MultipleNewsMessage: 106
};
