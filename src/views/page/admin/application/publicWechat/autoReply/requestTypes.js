export default {
  ImageRequest: 0,
  LinkRequest: 1,
  LocationRequest: 2,
  ShortVideoRequest: 3,
  TextRequest: 4,
  VideoRequest: 5,
  VoiceRequest: 6,
  EventRequest: 7,
  Event_ClickRequest: 8,
  Event_ViewRequest: 9,
  Event_SubscribeRequest: 10,
  Event_UnsubscribeRequest: 11,
  Event_ScanRequest: 12,
  Event_LocationRequest: 13
};
