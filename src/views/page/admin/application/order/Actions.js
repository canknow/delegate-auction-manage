export default {
  data () {
    return {
      passModel: {
        id: null,
        remark: null
      },
      denyModel: {
        id: null,
        reason: null
      },
      payModel: {

      },
      remarkModel: {
        id: null,
        remark: null
      },
      passRules: {},
      denyRules: {},
      payRules: {},
      remarkRules: {},
      showPassModal: false,
      passing: false,
      showDenyModal: false,
      denying: false,
      paying: false,
      remarking: false,
      compensatting: false,
      showPayModal: false,
      showRemarkModal: false
    };
  },
  methods: {
    reload () {
      this.search();
    },
    showRemark (item) {
      this.remarkModel.id = item.id;
      this.remarkModel.remark = null;
      this.showRemarkModal = true;
    },
    showPass (item) {
      this.passModel.id = item.id;
      this.passModel.remark = null;
      this.showPassModal = true;
    },
    showDeny (item) {
      this.denyModel.id = item.id;
      this.denyModel.reason = null;
      this.showDenyModal = true;
    },
    showPay (item) {
      this.payModel.id = item.id;
      this.showPayModal = true;
    },
    cancelPassModal () {
      this.showPassModal = false;
    },
    cancelDenyModal () {
      this.showDenyModal = false;
    },
    cancelPayModel () {
      this.showPayModal = false;
    },
    cancelRemarkModel () {
      this.showRemarkModal = false;
    },
    compensate (item) {
      this.compensatting = true;
      this.api.compensate({
        id: item.id
      }).then(() => {
        this.$Message.success(this.$t('app.common.message.successfullySaved'));
        this.reload();
      }).finally(() => {
        this.compensatting = false;
      });
    },
    pass () {
      this.$refs.passForm.validate((valid) => {
        if (valid) {
          this.passing = true;
          this.api.pass(this.passModel).then(() => {
            this.$Message.success(this.$t('app.common.message.successfullySaved'));
            this.reload();
            this.cancelPassModal();
          }).finally(() => {
            this.passing = false;
          });
        }
      });
    },
    deny () {
      this.$refs.denyForm.validate((valid) => {
        if (valid) {
          this.denying = true;
          this.api.deny(this.denyModel).then(() => {
            this.$Message.success(this.$t('app.common.message.successfullySaved'));
            this.reload();
            this.cancelDenyModal();
          }).finally(() => {
            this.denying = false;
          });
        }
      });
    },
    pay () {
      this.$refs.payForm.validate((valid) => {
        if (valid) {
          this.paying = true;
          this.api.pay(this.payModel).then(() => {
            this.$Message.success(this.$t('app.common.message.successfullySaved'));
            this.reload();
            this.cancelPayModel();
          }).finally(() => {
            this.paying = false;
          });
        }
      });
    },
    remark () {
      this.$refs.remarkForm.validate((valid) => {
        if (valid) {
          this.remarking = true;
          this.api.remark(this.remarkModel).then(() => {
            this.$Message.success(this.$t('app.common.message.successfullySaved'));
            this.reload();
            this.cancelRemarkModel();
          }).finally(() => {
            this.remarking = false;
          });
        }
      });
    }
  }
};
