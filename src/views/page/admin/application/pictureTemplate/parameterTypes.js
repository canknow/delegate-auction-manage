import pictureTemplateParameterType from './pictureTemplateParameterType';
import avatar from './../../../../../assets/images/avatar.png';
import qrcode from './../../../../../assets/images/qrcode.jpg';

export default [
  {
    name: 'avatar',
    value: 'avatar',
    demoPicture: avatar,
    type: pictureTemplateParameterType[1].value
  },
  {
    name: 'nickName',
    value: 'nickName',
    demoText: '小红帽',
    type: pictureTemplateParameterType[0].value
  },
  {
    name: 'Qrcode',
    value: 'qrcode',
    demoPicture: qrcode,
    type: pictureTemplateParameterType[1].value
  },
];
