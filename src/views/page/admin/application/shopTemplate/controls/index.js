import NavControl from './NavControl';
import PictureAdControl from './PictureAdControl';
import SliderControl from './SliderControl';
import ShopwindowControl from './ShopwindowControl';
import PackageControl from './PackageControl';
import TeamControl from './TeamControl';
import AuctionRecordControl from './AuctionRecordControl';
import NoticeControl from './NoticeControl';

export default {
  NavControl,
  PictureAdControl,
  SliderControl,
  ShopwindowControl,
  PackageControl,
  TeamControl,
  AuctionRecordControl,
  NoticeControl
};
