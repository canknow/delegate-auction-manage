import NavControlConfig from './NavControlConfig';
import PictureAdControlConfig from './PictureAdControlConfig';
import SliderControlConfig from './SliderControlConfig';
import ShopwindowControlConfig from './ShopwindowControlConfig';
import PackageControlConfig from './PackageControlConfig';
import TeamControlConfig from './TeamControlConfig';
import AuctionRecordControlConfig from './AuctionRecordControlConfig';
import NoticeControlConfig from './NoticeControlConfig';

export default {
  NavControlConfig,
  PictureAdControlConfig,
  SliderControlConfig,
  ShopwindowControlConfig,
  PackageControlConfig,
  TeamControlConfig,
  AuctionRecordControlConfig,
  NoticeControlConfig
};
