import Vue from 'vue';

import vueLangPlugin from './langPlugin';
import resultPlugin from './resultPlugin';
import userConfigurationPlugin from './userConfigurationPlugin';

Vue.use(vueLangPlugin);
Vue.use(resultPlugin);
Vue.use(userConfigurationPlugin);
