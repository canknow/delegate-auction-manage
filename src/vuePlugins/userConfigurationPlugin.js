import userConfigurationService from './../appServices/public/userConfigurationService';

export default {
  install: function (Vue, options) {
    Vue.mixin({
      methods: {
        loadAppConfiguration() {
          userConfigurationService.getUserMultiTenancyConfig().then((data) => {
            this.$store.commit('app/setMultiTenancy', data);
          });
          userConfigurationService.getUserFeaturesConfig().then((data) => {
            this.$store.commit('app/setAllFeatures', data.allFeatures);
            this.$store.commit('app/setFeatures', data.features);
          });
        },
        loadUserConfiguration () {
          userConfigurationService.getUserAuthConfig().then((data) => {
            this.$store.commit('session/setPermission', data);
          });
        }
      }
    });
  }
};
