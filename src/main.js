import Vue from 'vue';
import canknow from 'canknow';
import i18n from './langs';
import './vuePlugins';
import VueDND from 'awe-dnd';
import store from './store/index';
import router from './router/index';
import directives from './directives';
import Icon from 'canknow-icon/src/component/icon/index';
import filters from './filters';
import components from './views/components';
import assist from './utils/assist';
import App from './App';
import './utils/error-log';
import './styles/index.scss';
import 'canknow-icon/src/component/icon/src/icon.scss';

Vue.use(VueDND);
Vue.use(Icon);
Vue.use(canknow, {
  i18n: (key, value) => i18n.t(key, value),
  config: {
    baseUrl: assist.getInterfaceUrl()
  }
});
Vue.config.productionTip = false;

Object.keys(directives).forEach(key => {
  Vue.directive(key, directives[key]);
});
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key]);
});
Object.keys((components)).forEach(key => {
  Vue.component(key, components[key]);
});

new Vue({
  el: '#app',
  router,
  store,
  i18n,
  data: {
    currentPageName: ''
  },
  components: { App },
  render: h => h(App)
});
