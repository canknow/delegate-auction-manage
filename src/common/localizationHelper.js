import cookie from 'js-cookie';
import i18n from './../langs';

export function setCurrentLanguage (store, name) {
  i18n.locale = name;
  store.commit('localization/setCurrentLanguage', name);
  cookie.set(
    'Abp.Localization.CultureName',
    name,
    { expires: new Date(new Date().getTime() + 5 * 365 * 86400000) }
  );
}
