import store from '../store/index';

const baseUrl = function () {
  return window.location.protocol + '//' + store.state.session.tenant.domain + '/app/mobile/#';
};
const mobileLinkHelper = {
  getArticleLink (id) {
    return baseUrl() + '/article/content?id=' + id;
  },
  getArticleCategoryLink (id) {
    return baseUrl() + '/articleCategory/index?id=' + id;
  },
  getArticleHomeLink () {
    return baseUrl() + '/article/index';
  },
  getHomeLink () {
    return baseUrl() + '/home/index';
  },
  getUserCenterLink () {
    return baseUrl() + '/user/index';
  },
  getSpreadPosterLink () {
    return baseUrl() + '/spreadPoster/list';
  },
};
mobileLinkHelper.getLinkById = (name, id) => {
  return mobileLinkHelper[name](id);
};
export default mobileLinkHelper;
