export default class EditionPaymentType {
  static NewRegistration = 0;
  static BuyNow = 1;
  static Upgrade = 2;
  static Extend = 3;
}
