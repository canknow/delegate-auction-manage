export default class PaymentPeriodType {
  static Monthly = 30;
  static Annual = 365;
}
