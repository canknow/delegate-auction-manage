import cookie from 'js-cookie';
import appConfig from '../../config/app';
import assist from './../../utils/assist';

const authorization = {};

authorization.isAuthorized = () => {
  return !!localStorage.getItem('token');
};

authorization.getToken = () => {
  return localStorage.getItem('token');
};

authorization.setToken = (token, tokenExpireDate) => {
  localStorage.setItem('token', token);
};

authorization.clearToken = () => {
  localStorage.removeItem('token');
};

authorization.signIn = async (accessToken, tokenExpireDate) => {
  cookie.set(appConfig.authorization.encrptedAuthTokenName, accessToken, { expires: tokenExpireDate });
  authorization.setToken(accessToken, tokenExpireDate);
};

authorization.logout = async () => {
  authorization.clearToken();
  const store = require('../../store').default;
  await store.dispatch('tagsView/delAllViews');
  const router = require('../../router').default;
  router.push({ path: '/account/login' });
};

authorization.externalLogin = (authenticationScheme) => {
  const returnUrl = encodeURI(window.location.href);
  window.location.href = `${assist.getInterfaceUrl()}/admin/Account/ExternalLogin?provider=${authenticationScheme.name}&returnUrl=${returnUrl}`;
};

export default authorization;
