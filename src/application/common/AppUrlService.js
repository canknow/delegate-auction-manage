import store from './../../store/index';
import AppConsts from './../AppConsts';

class AppUrlService {
  static tenancyNamePlaceHolder = '{TENANCY_NAME}';

  get appRootUrl () {
    if (store.state.session.tenant) {
      return this.getAppRootUrlOfTenant(store.state.session.tenant.tenancyName);
    }
    else {
      return this.getAppRootUrlOfTenant(null);
    }
  }

  /**
   * Returning url ends with '/'.
   */
  getAppRootUrlOfTenant (tenancyName) {
    let baseUrl = this.ensureEndsWith(AppConsts.appBaseUrlFormat, '/');

    // Add base href if it is not configured in appconfig.json
    if (baseUrl.indexOf(AppConsts.appBaseHref) < 0) {
      baseUrl = baseUrl + this.removeFromStart(AppConsts.appBaseHref, '/');
    }

    if (baseUrl.indexOf(AppUrlService.tenancyNamePlaceHolder) < 0) {
      return baseUrl;
    }

    if (baseUrl.indexOf(AppUrlService.tenancyNamePlaceHolder + '.') >= 0) {
      baseUrl = baseUrl.replace(AppUrlService.tenancyNamePlaceHolder + '.', AppUrlService.tenancyNamePlaceHolder);
      if (tenancyName) {
        tenancyName = tenancyName + '.';
      }
    }

    if (!tenancyName) {
      return baseUrl.replace(AppUrlService.tenancyNamePlaceHolder, '');
    }
    return baseUrl.replace(AppUrlService.tenancyNamePlaceHolder, tenancyName);
  }

  ensureEndsWith (str, c) {
    if (str.charAt(str.length - 1) !== c) {
      str = str + c;
    }
    return str;
  }

  removeFromEnd (str, c) {
    if (str.charAt(str.length - 1) === c) {
      str = str.substr(0, str.length - 1);
    }
    return str;
  }

  removeFromStart (str, c) {
    if (str.charAt(0) === c) {
      str = str.substr(1, str.length - 1);
    }
    return str;
  }
}

export default new AppUrlService();
