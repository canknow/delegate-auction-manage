const appConfig = {
  name: 'system manage template',
  logo: '',
  version: '1.0',
  baseUrl: process.env.VUE_APP_API_BASE_URL,
  userManagement: {
    defaultAdminUserName: 'admin'
  },
  localization: {
    defaultLocalizationSourceName: 'Application'
  },
  authorization: {
    encrptedAuthTokenName: 'enc_auth_token'
  }
};
export default appConfig;
