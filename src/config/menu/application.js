export default [
  {
    name: 'CustomerIndex',
    icon: 'user',
    displayName: 'menu.customer',
    multiTenancySides: 1
  },
  {
    name: 'NoticeIndex',
    icon: 'activity',
    displayName: 'menu.notice',
    multiTenancySides: 1
  },
  {
    name: 'TeamIndex',
    icon: 'group',
    displayName: 'menu.team',
    multiTenancySides: 1
  },
  {
    name: 'PackageIndex',
    icon: 'package',
    displayName: 'menu.package',
    multiTenancySides: 1
  },
  {
    name: 'OrderIndex',
    icon: 'order',
    displayName: 'menu.order',
    multiTenancySides: 1
  },
  {
    name: 'AuctionRecordIndex',
    icon: 'auction',
    displayName: 'menu.auction',
    multiTenancySides: 1
  },
  {
    name: 'CommentIndex',
    icon: 'comment',
    displayName: 'menu.comment',
    multiTenancySides: 1
  },
  {
    name: 'ArticleIndex',
    icon: 'article',
    displayName: 'menu.article',
    multiTenancySides: 1
  },
  {
    name: 'ShopIndex',
    icon: 'shop',
    displayName: 'menu.shop',
    multiTenancySides: 1
  },
  {
    name: 'ShopTemplateIndex',
    icon: 'template',
    displayName: 'menu.shopTemplate',
    multiTenancySides: 1
  },
  {
    name: 'BidIndex',
    icon: 'bid',
    displayName: 'menu.bid',
    multiTenancySides: 1
  },
  {
    name: 'SpreadPosterTemplateIndex',
    icon: 'template',
    displayName: 'menu.spreadPosterTemplate',
    multiTenancySides: 1
  },
  {
    name: 'QuestionAndAnswerIndex',
    icon: 'question',
    displayName: 'menu.questionAndAnswer',
    multiTenancySides: 1
  },
  {
    name: 'publicWechat',
    icon: 'wechat',
    displayName: 'menu.publicWechat',
    multiTenancySides: 1,
    items: [
      {
        name: 'PublicWechatSetting',
        icon: 'bid',
        displayName: 'menu.setting',
        multiTenancySides: 1,
      },
      {
        name: 'PublicWechatFile',
        icon: 'file',
        displayName: 'menu.file',
        multiTenancySides: 1,
      },
      {
        name: 'PublicWechatMenu',
        icon: 'bid',
        displayName: 'menu.menu',
        multiTenancySides: 1,
      },
      {
        name: 'PublicWechatAutoReply',
        icon: 'bid',
        displayName: 'menu.autoReply',
        multiTenancySides: 1,
      },
    ]
  },
  {
    name: 'wallet',
    icon: 'wallet',
    displayName: 'menu.wallet',
    multiTenancySides: 1,
    items: [
      {
        name: 'WalletWalletRecord',
        displayName: 'menu.walletRecord',
        multiTenancySides: 1,
      },
      {
        name: 'WalletWithdrawApply',
        displayName: 'menu.withdrawApply',
        multiTenancySides: 1,
      },
      {
        name: 'FinanceBankIndex',
        displayName: 'menu.bank',
        multiTenancySides: 1,
      },
    ]
  }
];
