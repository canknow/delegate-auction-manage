import common from './common';
import application from './application';

export default [
  ...common,
  ...application
];
