export default [
  {
    'name': 'Administration',
    'icon': 'setting',
    'displayName': 'menu.system',
    'items': [
      {
        'name': 'Administration.Dashboard.Host',
        'displayName': 'menu.dashboard',
        'permissions': ['Pages.Administration.Host'],
      },
      {
        'name': 'Administration.Dashboard.Tenant',
        'displayName': 'menu.dashboard',
        'permissions': ['Pages.Administration.Tenant'],
      },
      {
        'name': 'Administration.User',
        'displayName': 'menu.user',
        'permissions': ['Pages.Administration.Common.Users'],
      },
      {
        'name': 'Administration.Tenants',
        'displayName': 'menu.tenant',
        'permissions': ['Pages.Administration.Host.Tenants'],
      },
      {
        'name': 'Administration.File',
        'displayName': 'menu.file',
        'permissions': ['Pages.Administration.Common.Files'],
      },
      {
        'name': 'Administration.Setting.Host',
        'displayName': 'menu.setting',
        'permissions': ['Pages.Administration.Host'],
      },
      {
        'name': 'Administration.Setting.Tenant',
        'displayName': 'menu.setting',
        'permissions': ['Pages.Administration.Tenant'],
      },
      {
        'name': 'Administration.OrganizationUnits',
        'displayName': 'menu.organizationUnits',
        'permissions': ['Pages.Administration.Common.OrganizationUnits'],
      },
      {
        'name': 'Administration.Edition',
        'displayName': 'menu.edition',
        'permissions': ['Pages.Administration.Host.Editions'],
      },
      {
        'name': 'Administration.Maintenance',
        'displayName': 'menu.maintenance',
        'permissions': ['Pages.Administration.Host.Maintenance'],
      },
      {
        'name': 'Administration.BackgroundJob',
        'displayName': 'menu.backgroundJob',
        'permissions': ['Pages.Administration.Host.BackgroundJob']
      },
      {
        'name': 'Administration.AuditLog',
        'displayName': 'menu.auditLog',
        'permissions': ['Pages.Administration.Host.AuditLogs']
      },
      {
        'name': 'Administration.Captcha',
        'displayName': 'menu.captcha',
        multiTenancySides: 2
      },
      {
        'name': 'Administration.BusinessNotifier',
        'displayName': 'menu.businessNotifier',
        multiTenancySides: 1
      }
    ]
  }
];
