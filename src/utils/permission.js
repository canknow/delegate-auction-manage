import store from './../store';

export function hasPermission (permissionName) {
  return store.state.session.permission.allPermissions[permissionName] !== undefined
    && store.state.session.permission.grantedPermissions[permissionName] !== undefined;
}

export function hasAllOfPermissions () {
  return areAllGranted.apply(this, arguments);
}

export function isAnyGranted () {
  if (!arguments || arguments.length <= 0) {
    return true;
  }

  for (let i = 0; i < arguments.length; i++) {
    if (hasPermission(arguments[i])) {
      return true;
    }
  }

  return false;
}

export function areAllGranted () {
  if (!arguments || arguments.length <= 0) {
    return true;
  }

  for (let i = 0; i < arguments.length; i++) {
    if (!hasPermission(arguments[i])) {
      return false;
    }
  }
  return true;
}
