import application from './application/zh-CN';
import menu from './menu/zh-CN';
import common from './common/zh-CN';

export default {
  app: {
    common,
    application
  },
  menu
};
