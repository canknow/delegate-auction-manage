FROM node:8-slim
MAINTAINER canknow <3230525823@qq.com>
RUN apt-get update && apt-get install -y nginx
WORKDIR /app
COPY . /app/
EXPOSE 80
RUN npm install yarn && yarn && npm run build && cp -r dist/* /var/www/html && rm -rf /app
CMD ["nginx","-g", "daemon off;"]
