const { notEmpty } = require('../utils.js');

module.exports = {
  description: 'generate a view',
  prompts: [{
    type: 'input',
    name: 'name',
    message: 'view name please',
    validate: notEmpty('name')
  }],
  actions: data => {
    const name = '{{name}}';
    const actions = [{
      type: 'add',
      path: `src/views/page/admin/application/${name}/Index.vue`,
      templateFile: 'plop-templates/crudSpa/Index.hbs',
      data: {
        name: name,
        templateStart: '{{',
        templateEnd: '}}'
      }
    }, {
      type: 'add',
      path: `src/views/page/admin/application/${name}/CreateOrUpdateModal.vue`,
      templateFile: 'plop-templates/crudSpa/CreateOrUpdateModal.hbs',
      data: {
        name: name,
        templateStart: '{{',
        templateEnd: '}}'
      }
    }, {
      type: 'add',
      path: `src/views/page/admin/application/${name}/index.scss`,
      templateFile: 'plop-templates/crudSpa/index.scss',
      data: {
        name: name
      }
    }, {
      type: 'add',
      path: `src/appServices/admin/application/${name}.js`,
      templateFile: 'plop-templates/crudSpa/api.js',
      data: {
        name: name
      }
    }, {
      type: 'add',
      path: `src/router/routers/admin/application/${name}.js`,
      templateFile: 'plop-templates/crudSpa/router.tlp',
      data: {
        name: name
      }
    }];
    return actions;
  }
};
